from setuptools import setup, find_packages
import sys

#sys.path.append("./src")

import genbot

setup(name="genbot",
        version=genbot.__version__,
        description="BuildBot generation CI",
        long_description='''
        Generate BuildBot master files to run as a service
        ''',
        author="Jason Kenny",
        author_email="dragon512@live.com",
        url="https://bitbucket.org/dragon512/genbot",
        license="MIT",
        packages=find_packages(include='genbot*'),
        entry_points={
          'console_scripts': [
            'genbot = genbot.__main__:main',
            'genbot-docker-registry = genbot.scripts.registry_check:main',
            'genbot-docker-tls-gen = genbot.scripts.gen_docker_tls:main',
            ]
          },
        install_requires =[
            "buildbot[bundle,tls]",
            "pyyaml",
            'docker',
            'jinja2',
            'requests',
            'autopep8',
            ],

        package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt'],

        },

        # see classifiers
        # http://pypi.python.org/pypi?%3Aaction=list_classifiers
         classifiers=[
            'Development Status :: 5 - Production/Stable',
            'Environment :: Console',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: MIT License',
            'Operating System :: OS Independent',
            'Programming Language :: Python :: 2',
            'Programming Language :: Python :: 2.7',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.4',
            'Programming Language :: Python :: 3.5',
            'Programming Language :: Python :: 3.6',
            'Topic :: Terminals',
            ],
)

import genbot.interfaces as interfaces
import genbot.glb as glb
import genbot.common.is_a as is_a

import genbot.reporters as reporters


def createReporter(typename, info, dom):

    # this should be an dictionary with one key (which is a type)
    # it should only have 1 type
    
    try:
        # create the objec the type
        obj = glb.known_reporters[typename](dom)

    except KeyError:
        raise ValueError(
            "{typename} is not a valid value for Reporter type".format(typename=typename))

    values = obj.get_defaults(dom.Defaults)
    
    if info is False:
        data = {'pipelines':[]} # nothing is reported
    elif info is True:
        data = {'pipelines':None} # everything is reported
    values.update(info) # override defaults
    obj.parse(values,typename)
    
    return obj
from __future__ import absolute_import, division, print_function

from twisted.internet import defer
from zope.interface import implementer

from buildbot.interfaces import ITriggerableScheduler
from buildbot.process.properties import Properties
import buildbot.schedulers.triggerable as triggerable


@implementer(ITriggerableScheduler)
class Triggerable(triggerable.Triggerable):

    def __init__(self, name, builderNames, **kw):
        super(Triggerable, self).__init__(name, builderNames, **kw)

    @defer.inlineCallbacks
    def addBuildsetForSourceStampsWithDefaults(self, reason, sourcestamps=None,
                                               waited_for=False, properties=None, builderNames=None,
                                               **kw):
        new_codebases = {}
        # this override goes over the source stamps and looks
        for stamp in sourcestamps:
            new_codebases[stamp['codebase']] = {}
        self.codebases = new_codebases
      
        ret = yield super(Triggerable, self).addBuildsetForSourceStampsWithDefaults(
            reason=reason,
            sourcestamps=sourcestamps,
            waited_for=waited_for,
            properties=properties,
            builderNames=builderNames,
            **kw
        )

        defer.returnValue(ret)

'''
This script generates a checksum based on all the files in the directory and below
'''
from __future__ import absolute_import, division, print_function
import hashlib
import os
import argparse

def get_checksum(directory):
    md5 = hashlib.md5()
    # get all files
    files = os.listdir(directory)
    for x in files:
        fullsrc = os.path.join(directory, x)
        if os.path.isfile(fullsrc):
            with open(fullsrc,"r") as infile:
                md5.update(infile.read())
        elif os.path.isdir(fullsrc):
            tmp = get_checksum(fullsrc)
            md5.update(tmp)
    return md5.hexdigest()


def _path(exists, arg):
    path = os.path.abspath(arg)
    if not os.path.exists(path) and exists:
        msg = '"{0}" is not a valid path'.format(path)
        raise argparse.ArgumentTypeError(msg)
    return path


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--directory", "-D",
        required=True,
        type=lambda x: _path(True,x),
        help="directory to get Checksum on"
    )

    args = parser.parse_args()

    checksum = get_checksum(args.directory)
    print("{}:{}".format(os.path.basename(args.directory),checksum))

if __name__ == '__main__':    
    main()

from __future__ import absolute_import, division, print_function


from genbot.builders.image_builder import BuildDockerImage
from genbot.builders.pipelinestep import PipelineBuilder
from genbot.builders.generalbuilder import GeneralBuilder
from genbot.overrides import *

import genbot.common.is_a as is_a

from twisted.python import log
import pprint

class PrFilter(object):
    def __init__(self,users,**kw):
        if is_a.String(users):
            users=[users]
        self._users = users

    def __call__(self,data):
        user = data.get('user',{}).get('login','')
        number = data.get('number',"Unknown")
        if user in self._users:
            log.msg("User '{name}' in white list, PR {number} will be built".format(name=user,number=number))
            return True
        log.msg("User '{name}' not in white list, PR {number} change will be denied!".format(name=user,number=number))
        return False



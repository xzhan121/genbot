from __future__ import absolute_import, division, print_function

import os
import stat
import yaml
import argparse
import subprocess
import shutil


import genbot.pipeline as pipeline
import pprint

import genbot
from genbot.cidom import CiDom

import genbot.template as template
import genbot.common.namespace as namespace
import genbot.common.disk as disk
import genbot.sourceinfo as sourceinfo

def GenerateSingleUserCfgs(source_list, oufile):
    dom = CiDom()

    for src in source_list:
        filename = "_vcs/{name}/{file}".format(name=src.name,file=src.file)
        print("loading {0}".format(filename))
        with open(filename) as f:
            out = f.read()
        
        # add to DOM
        dom.load(out, src)
    # generate the file
    GenerateSingleUserCfg(dom, oufile)

def GenerateDockerWorkerCfg(cfgdata):

    path = os.path.join(os.path.split(cfgdata.master.filename)[0],"docker_workers")
    if not os.path.exists(path):
        os.makedirs(path)

    # with open(os.path.join(path,"buildbot.tac"), "w") as outfile:
        # outfile.write(template.buildbot_tac)

    # with open(os.path.join(path,"dockerfile.in"), "w") as outfile:
        # outfile.write(template.dockerfile.render(**cfgdata))

    install_path = os.path.dirname(__file__)

    shutil.copy(os.path.join(install_path,"scripts","dir_checksum.py"),path)
    shutil.copy(os.path.join(install_path,"scripts","gen_dockerfile.py"),path)

def GenerateSingleUserCfg(dom, cfgdata):

    path, tmp = os.path.split(cfgdata.master.filename)
    if path and not os.path.exists(path):
        os.makedirs(path)

#    print("*************************************")
    
    #print(dom.Dump())
    #print("*************************************")

    # # output header
    # print(template.header.render(**cfgdata))
    # print(template.code_base_generator.render(**cfgdata))

    # # output workers
    # print(template.image_workers.render(**cfgdata))
    # print(template.pipeline_workers.render(**cfgdata))
    # print(template.docker_workers.render(**cfgdata))
    # print(template.fixed_workers.render(**cfgdata))

    # # output Change sources (poll types)
    # print(dom.getChangeSources())

    # # ##############################
    # # output schedulers
    # print(template.image_trigger.render(**cfgdata))
    # print(template.generic_builder_trigger.render(**cfgdata))
    # # output user schedulers
    print(dom.getSchedulersSource())

    # # output builders (steps/piplines)
    # print(template.pipeline_builder.render(**cfgdata))
    # print(template.image_builder.render(**cfgdata))
    # print(template.generic_builder.render(**cfgdata))

    # # reporters/services

    # # web UI / Auth / push change sources
    # print(template.basic_www.render(**cfgdata))

    # # DB
    # print(template.db_url.render())
    
    with open(cfgdata.master.filename, "w") as outfile:

        # output header
        outfile.write(template.header.render(**cfgdata))
        outfile.write(template.code_base_generator.render(**cfgdata))

        # output workers
        outfile.write(template.image_workers.render(**cfgdata))
        outfile.write(template.pipeline_workers.render(**cfgdata))
        outfile.write(template.docker_workers.render(**cfgdata))
        outfile.write(template.fixed_workers.render(**cfgdata))

        # output Change sources (poll types)
        outfile.write(dom.getChangeSources())

        ##############################
        # output schedulers
        outfile.write(template.image_trigger.render(**cfgdata))
        outfile.write(template.generic_builder_trigger.render(**cfgdata))
        # output user schedulers
        outfile.write(dom.getSchedulersSource())

        # output builders (steps/piplines)
        outfile.write(template.pipeline_builder.render(**cfgdata))
        outfile.write(template.image_builder.render(**cfgdata))
        outfile.write(template.generic_builder.render(**cfgdata))

        # reporters/services

        # web UI / Auth / push change sources
        outfile.write(template.basic_www.render(**cfgdata))

        # DB
        outfile.write(template.db_url.render())

    subprocess.call(['autopep8', '-i', cfgdata.master.filename])

def read_cfg(infile):
    print("loading config file {0}".format(infile))
    with open(infile) as f:
        out = f.read()
    # load new data

        data = namespace.Namespace(yaml.load(out))
        import pprint
        # print(pprint.pformat(data))
        return data
    return {}

def GenerateBaseImage(cfgdata):
    path = os.path.join(os.path.split(cfgdata.master.filename)[0],"basic_image")
    # if not os.path.exists(path):
        # os.makedirs(path)

    # with open(os.path.join(path,"buildbot.tac"), "w") as outfile:
        # outfile.write(template.buildbot_tac)

    # with open(os.path.join(path,"Dockerfile"), "w") as outfile:
        # outfile.write(template.basic_dockerfile.render(**cfgdata))
    
    script_file=os.path.join(path,"genscript.sh")
    # with open(script_file, "w") as outfile:
        # outfile.write(template.gen_image.render(**cfgdata))
    # st = os.stat(script_file)
    # os.chmod(script_file, st.st_mode | stat.S_IEXEC)    

    install_path = os.path.dirname(__file__)

    shutil.copy(os.path.join(install_path,"scripts",'bbdockerworker',"docker-entrypoint.sh"),path)
    shutil.copy(os.path.join(install_path,"scripts",'bbdockerworker',"modprobe.sh"),path)
    

def get_sources(data):
    ret=[]
    if os.path.exists("_vcs"):
        shutil.rmtree("_vcs", onerror=disk.remove_read_only)
    for source in data.sources:

        name = source.keys()[0]
        info = source[name]
        print("Getting information from {0} branch {1}".format(info.repo,info.branch))

        if subprocess.call(["git","clone","--single-branch", "--depth","1" ,"-b",info.branch, info.repo, "_vcs/"+name]):
            print("Failed to get information from {0} branch {1}".format(info.repo,info.branch))
        else:
            build_info=sourceinfo.SourceInfo(
                name=name,
                repo = info.repo,
                branch = info.branch,
                yaml_file=info.yaml_file
            )
            ret.append(build_info)
    return ret


def main():    
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--cfg-file",
                        nargs='?',
                        default="genbot.yaml",
                        help="Config file to define custom data")

    parser.add_argument("--multimaster",
                        action='store_true',
                        default=False,
                        help="generate a multimaster config file")
   
    parser.add_argument('-V', '--version', action='version',
                        version='%(prog)s {0}'.format(genbot.__version__))

    args = parser.parse_args()

    # read config
    config_data = read_cfg(args.cfg_file)

    # get sources to read the file
    input_yamls = get_sources(config_data)
    if not input_yamls:
        print("No Input files defined")
        exit(1)

    if args.multimaster:
        print("Not impled yet")
        exit(1)

    GenerateSingleUserCfgs(input_yamls, config_data)
    GenerateDockerWorkerCfg(config_data)
    GenerateBaseImage(config_data)
    


from __future__ import absolute_import, division, print_function
import interfaces


def Pipelines(obj,pipeline_map):
    for k,v in obj.items():
        p=Pipeline()
        p.parse(v,k)
        pipeline_map[k]=v




from __future__ import absolute_import, division, print_function
import abc

from buildbot.plugins import *

import genbot.common.is_a as is_a
from genbot.argument import Types, Argument


class Base(object):

    def __init__(self, dom):
        # print("called __init__(): Base")
        self._name = None
        self._args_def = set([])
        self._args = {}
        self._build_info = dom.buildinfo
        self._dom = dom

    @property
    def buildinfo(self):
        return self._build_info

    @property
    def dom(self):
        return self._dom

    def add_arg_def(self, obj):
        self._args_def |= set(obj)

    @abc.abstractmethod
    def getObject(self):
        '''return the buildbot object'''
        raise NotImplementedError

    def get_defaults(self, defaults):
        return {}

    def parse(self, obj, id=None):
        for key, value in obj.items():
            tmp = self.verify_args(key, value)
            if tmp:
                if tmp.ArgName == "name":
                    self._name = value
                    # will add something for cases in which
                    # we cannot map name as arg
                    self._args[tmp.ArgName] = value
                else:
                    self._args[tmp.ArgName] = value

    def verify_args(self, arg, value):

        has_arg = False
        ret = None
        for arg_def in self._args_def:
            if arg in arg_def:
                has_arg = True
                verify_value = arg_def.verify(value)
                ret = arg_def
                break
        if not has_arg:
            print ('Invalid argument "{0}"'.format(arg))
            return None
        elif verify_value:
            print(verify_value.format(arg))
            return None
        return ret

    def args_as_source(self):
        ret = ''
        for k, v in self._args.items():
            if is_a.String(v):
                ret += "{k}='{v}',".format(k=k, v=v)
            else:
                ret += "{k}={v},".format(k=k, v=v)
        return ret

    @property
    def name(self):
        return self._name


class Step(Base):

    def __init__(self, dom):
        super(Step, self).__init__(dom)
        self.add_arg_def(
            [
                Argument('name', Types.String, required=True),
                Argument('haltOnFailure', Types.Boolean),
                Argument('flunkOnWarnings', Types.Boolean),
                Argument('flunkOnFailure', Types.Boolean),
                Argument('warnOnWarnings', Types.Boolean),
                Argument('warnOnFailure', Types.Boolean),
                Argument('alwaysRun', Types.Boolean),
                Argument('description', Types.String),
                Argument('descriptionDone', Types.String),
                Argument('descriptionSuffix', Types.String),
                Argument('doStepIf', Types.Any),
                Argument('hideStepIf', Types.Any),
                Argument('locks', Types.Any),
                Argument('logEncoding', Types.String),
                Argument('updateBuildSummaryPolicy', Types.Any)
            ]
        )

    def get_defaults(self, defaults):
        default_values = super(Step, self).get_defaults(defaults)
        default_values.update(
            defaults.get("Step", {}))
        return default_values


class SourceCheckout(Step):

    def __init__(self, dom):
        # print("called __init__(): SourceCheckout")
        super(SourceCheckout, self).__init__(dom)

        self.add_arg_def([
            Argument('mode', Types.String),  # make enum
            Argument('method', Types.String),  # make enum
            Argument('workdir', Types.String),
            Argument('alwaysUseLatest', Types.Boolean),
            # Argument('retry',Types.IntegerArray), # need to make some sort of mapping or converter to a tuple
            Argument('repository', Types.String),
            Argument('codebase', Types.String),
            Argument('timeout', Types.Number),
            Argument('logEnviron', Types.Boolean),
            Argument('env', Types.Object),
        ])

    def get_defaults(self, defaults):
        default_values = super(SourceCheckout, self).get_defaults(defaults)
        default_values.update(
            defaults.get("SourceCheckout", {}))
        return default_values


class Environment(Base):
    def __init__(self, dom):
        super(Environment, self).__init__(dom)
        self.add_arg_def([
            Argument('default', Types.String),
        ])

        self._default=True

    def parse(self, obj, id=None):
        super(Environment, self).parse(obj, id)
        self._default = self._args.get('default',True)


    @property
    def isDefault(self):
        return self._default

class Schedule(Base):

    def __init__(self, dom):
        super(Schedule, self).__init__(dom)
        self.add_arg_def([
            Argument('name', Types.String, required=True),
            Argument('codebases', [Types.ObjectArray, Types.StringArray]),
            Argument('triggers', Types.StringArray,
                     arg_name='builderNames', required=True),
        ])

    def get_defaults(self, defaults):
        default_values = super(Schedule, self).get_defaults(defaults)
        default_values.update(
            defaults.get("Schedule", {}))
        return default_values


class Reporter(Base):
    def __init__(self, dom):
        super(Reporter, self).__init__(dom)
        self.add_arg_def([
            Argument('pipelines', Types.StringArray),            
        ])

    def get_defaults(self, defaults):
        default_values = super(Reporter, self).get_defaults(defaults)
        default_values.update(
            defaults.get("Reporter", {}))
        return default_values



class ChangeSource(Base):

    def __init__(self, dom):
        super(ChangeSource, self).__init__(dom)
    
    def get_defaults(self, defaults):
        default_values = super(ChangeSource, self).get_defaults(defaults)
        default_values.update(
            defaults.get("ChangeSource", {}))
        return default_values
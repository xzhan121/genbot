from __future__ import absolute_import, division, print_function

from buildbot.process.properties import Interpolate, WithProperties

import genbot.common.is_a as is_a

def subst_type(obj):
    return isinstance(obj, Interpolate) or isinstance(obj, WithProperties)


class Types:
    Array = 1  # this bit set
    String = 1 << 1
    Integer = 2 << 1
    Number = 3 << 1
    Boolean = 4 << 1    
    Object = 5 << 1  # dictionary
    StringArray = Array | String
    IntegerArray = Array | Integer
    NumberArray = Array | Number
    BooleanArray = Array | Boolean
    ObjectArray = Array | Object
    Any = 100 << 1

    @classmethod
    def ErrorString(cls, types):
        ret = '{0} must be:\n '
        for cnt, t in enumerate(types):
            if t == Types.Array:
                ret += "a list"
            elif t == Types.String:
                ret += "a string"
            elif t == Types.Integer:
                ret += "an integer"
            elif t == Types.Number:
                ret += "a number"
            elif t == Types.Boolean:
                ret += "a Boolean"
            elif t == Types.Object:
                ret += "a object"
            elif t == Types.StringArray:
                ret += "a list of strings"
            elif t == Types.IntegerArray:
                ret += "a list of integers"
            elif t == Types.NumberArray:
                ret += "a list of numbers"
            elif t == Types.BooleanArray:
                ret += "a list of booleans"
            elif t == Types.ObjectArray:
                ret += "a list of objects"
            if cnt < len(types) - 1:
                ret += " or\n "
        return ret


class Argument(object):
    def __init__(self, keys, types, arg_name=None, required=False):
        self._keys = keys if is_a.List(keys) else [keys]
        self._arg_name = arg_name if arg_name else self._keys[0]
        self._types = types if is_a.List(types) else [types]
        self._required = required

    @property
    def ArgName(self):
        return self._arg_name

    @property
    def required(self):
        return self._required

    def __contains__(self, value):
        return value in self._keys

    def verify(self, value):
        type_match = False
        for t in self._types:
            if t == Types.Array and is_a.List(value):
                type_match = True
            elif t == Types.String and is_a.String(value) or subst_type(value):
                type_match = True
            elif t == Types.Integer and is_a.Int(value):
                type_match = True
            elif t == Types.Number and is_a.Number(value):
                type_match = True
            elif t == Types.Boolean and (is_a.Boolean(value) or str(value).lower() == "true" or str(value).lower() == "false"):
                type_match = True
            elif t == Types.Object and is_a.Dictionary(value):
                type_match = True
            elif t == Types.StringArray and is_a.ListOfStrings(value,[Interpolate,WithProperties]):
                type_match = True
            elif t == Types.IntegerArray and is_a.ListOfInts(value):
                type_match = True
            elif t == Types.NumberArray and is_a.ListOfNumbers(value,):
                type_match = True
            elif t == Types.BooleanArray and is_a.ListOfBooleans(value):
                type_match = True
            elif t == Types.ObjectArray and is_a.ListOfDictionary(value):
                type_match = True
            elif t == Types.Any:
                pass  # the type is implied to be ok

        if not type_match:
            return Types.ErrorString(self._types)

        return None

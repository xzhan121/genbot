from __future__ import absolute_import, division, print_function
import genbot.common.is_a as is_a


class Namespace(dict):

    def __init__(self, kw):
        new_kw = {}
        for k, v in kw.items():
            if is_a.Dict(v):
                new_kw[k] = Namespace(v)
            elif is_a.OrderedSequence(v):
                new_kw[k] = self._setup_list(v)
            else:
                new_kw[k] = v
        super(Namespace, self).__init__(new_kw)

    def _setup_list(self, lst):
        new_list=[]
        for i in lst:
            if is_a.Dict(i):
                new_list.append(Namespace(i))
            elif is_a.OrderedSequence(i):
                new_list.append(self._setup_list(i))
            else:
                new_list.append(i)
        return new_list
    
    def __getattr__(self, name):
        ''' This is ugly but because SCons does not have a good recursive subst
        code, I need to subst stuff here before SCons can try to, else it will
        try to set this object to Null string, causing an unwanted error'''
        try:
            tmp = self[name]
        except KeyError:
            raise AttributeError()
        return tmp

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        del self[name]

from __future__ import absolute_import, division, print_function
from builtins import int
from future.utils import native_str


def List(obj):
    return isinstance(obj, list)

def _isinstanceof(obj, types):    
    return any(isinstance(obj,t) for t in types)           

def ListOf(obj,types):
    if not OrderedSequence(types):
        types = [types]
    if isinstance(obj, list):
        for i in obj:
            if not _isinstanceof(i,types):
                return False
        return True
    return False

def Tuple(obj):
    return isinstance(obj, tuple)

def ListOfTuples(obj):
    return ListOf(obj,tuple)

def OrderedSequence(obj):
    return List(obj) or Tuple(obj)

def String(obj):
    return isinstance(obj, native_str)

def ListOfStrings(obj,strtype=[]):
    strtype.append(native_str)
    return ListOf(obj,strtype)

def Int(obj):
    return isinstance(obj, int)

def ListOfInts(obj):
    return ListOf(obj,int)

def Number(obj):
    return not isinstance(obj, bool) and (isinstance(obj, int) or isinstance(obj, float))

def ListOfNumbers(obj):
    return ListOf(obj,(bool,int,float))

def Boolean(obj):
    return isinstance(obj, bool)

def ListOfBooleans(obj):
    return ListOf(obj,bool)

def Dictionary(obj):
    return isinstance(obj, dict)

def ListOfDictionary(obj):
    return ListOf(obj,dict)

def Dict(obj):
    return isinstance(obj, dict)

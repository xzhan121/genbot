from __future__ import absolute_import, division, print_function

from buildbot.plugins import steps
import genbot.interfaces as interfaces
import genbot.api as api
from genbot.argument import Types, Argument
import genbot.common.is_a as is_a

class ShellCommand(interfaces.Step):
    def __init__(self,dom):
        super(ShellCommand, self).__init__(dom)
        self.add_arg_def([
            Argument('command',[Types.String,Types.StringArray], required=True),
            Argument('workdir',Types.String),
            Argument('env',Types.Object),
            Argument('logfiles',Types.Any),
            Argument('timeout',Types.Number),
            Argument('maxTime',Types.Number),
            Argument('logEniron',Types.Any),
            Argument('decodeRC',Types.Any)
        ])

    def getObject(self):
        return steps.ShellCommand(**self._args)

    def getSource(self):
        return "steps.ShellCommand(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):        
        default_values = super(ShellCommand, self).get_defaults(defaults)
        default_values.update(
            defaults.get("ShellCommand", {}))
        return default_values

    def parse(self, obj, id=None):
        super(ShellCommand, self).parse(obj, id)
        if self._args.get('name') is None and self._args.get('command') is not None:
            cmd = self._args['command']
            if is_a.List(cmd):
                cmd=cmd[0]
            else:
                cmd=cmd
            self._args['name'] = cmd


class Configure(ShellCommand):
    def __init__(self):
        super(Configure, self).__init__()

    def getObject(self):
        return steps.Configure(**self._args)

    def getSource(self):
        return "steps.Configure(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(Configure, self).get_defaults(defaults)
        default_values.update(
            defaults.get("Configure", {}))
        return default_values


class Test(ShellCommand):
    def __init__(self):
        super(Test, self).__init__()

    def getObject(self):
        return steps.Test(**self._args)

    def getSource(self):
        return "steps.Test(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(Test, self).get_defaults(defaults)
        default_values.update(
            defaults.get("Test", {}))
        return default_values


class SubunitShellCommand(ShellCommand):
    def __init__(self):
        super(SubunitShellCommand, self).__init__()


    def getObject(self):
        return steps.SubunitShellCommand(**self._args)

    def getSource(self):
        return "steps.SubunitShellCommand(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(SubunitShellCommand, self).get_defaults(defaults)
        default_values.update(
            defaults.get("SubunitShellCommand", {}))
        return default_values



api.DefineBuildStep(ShellCommand)
api.DefineBuildStep(Configure)
api.DefineBuildStep(Test)
api.DefineBuildStep(SubunitShellCommand)

from __future__ import absolute_import, division, print_function

import buildbot.util as Util
import genbot.api as api
import genbot.interfaces as interfaces
from buildbot.plugins import *
from buildbot.plugins import steps
from buildbot.process.properties import Interpolate
from genbot.argument import Argument, Types


def transform_url(type,orginal_repo):
    if not orginal_repo:
        return orginal_repo
    repo=orginal_repo
    info = Util.giturlparse(orginal_repo)
    if type == "git":
        repo = "git@{0}:{1}/{2}".format(info.domain, info.owner, info.repo)
    elif type == 'https':
        repo = "https://{0}/{1}/{2}".format(tmp.domain, tmp.owner, tmp.repo)
    # we we leave it alone
    return repo


class GitHub(interfaces.SourceCheckout):
    def __init__(self, dom):
        super(GitHub, self).__init__(dom)

        self.add_arg_def([
            Argument('repourl', Types.String),
            Argument('branch', Types.String),
            Argument('submodules', Types.String),
            Argument('shallow', Types.Integer),
            Argument('retryFetch', Types.Boolean),
            Argument('clobebrOnFailure', Types.Boolean),
            Argument('url_style', Types.String),
        ])

    def getObject(self):
        return steps.GitHub(**self._args)

    def getSource(self):
        return "steps.GitHub(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(GitHub, self).get_defaults(defaults)
        default_values.update(
            defaults.get("GitHub", {}))
        return default_values

    def parse(self, obj):

        super(GitHub, self).parse(obj)

        git_style = None
        if 'url_style' in self._args:
            git_style = self._args['url_style']
            del self._args['url_style']

        if "codebase" in self._args and 'repourl' not in self._args:
            # need to abstract Interpolate... TODO
            self._args['repourl'] = util.Transform(
                lambda x: transform_url(git_style,x), Interpolate(
                    '%(src:{codebase}:repository)s'.format(
                        codebase=self._args["codebase"])
                )
            )


api.DefineBuildStep(GitHub)

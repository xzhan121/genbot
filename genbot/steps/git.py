from __future__ import absolute_import, division, print_function

import genbot.api as api
import genbot.interfaces as interfaces
from buildbot.plugins import *
from buildbot.plugins import steps
from genbot.argument import Argument, Types


class Git(interfaces.SourceCheckout):
    def __init__(self,dom):
        super(Git, self,).__init__(dom)
        self.add_arg_def([
            Argument('repourl',Types.String),
            Argument('branch',Types.String),
            Argument('submodules',Types.String),
            Argument('shallow',Types.Integer),
            Argument('retryFetch',Types.Boolean),
            Argument('clobebrOnFailure',Types.Boolean),
        ])

    def getObject(self):
        return steps.Git(**self._args)

    def getSource(self):
        return "steps.Git(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(Git, self).get_defaults(defaults)
        default_values.update(
            defaults.get("Git", {}))
        return default_values

    def parse(self, obj):
        super(Git, self).parse(obj)
        if "codebase" in self._args and 'repourl' not in self._args:
            # need to abstract Interpolate... TODO            
            self._args['repourl'] = util.Transform(
                as_git_repo, Interpolate(
                    '%(src:{codebase}:repository)s'.format(
                        codebase=self._args["codebase"])
                )
            )

api.DefineBuildStep(Git)

from __future__ import absolute_import, division, print_function
from buildbot.plugins import steps
import genbot.interfaces as interfaces
import genbot.api as api
from genbot.argument import Types, Argument


class FileExists(interfaces.Step):
    def __init__(self, dom):
        super(FileExists, self).__init__(dom)
        self.add_arg_def([
            Argument('file', Types.String, required=True)
        ])

    def getObject(self):
        return steps.FileExists(**self._args)

    def getSource(self):
        return "steps.FileExists(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(FileExists, self).get_defaults(defaults)
        default_values.update(
            defaults.get("FileExists", {}))
        return default_values


class CopyDirectory(interfaces.Step):
    def __init__(self, dom):
        super(CopyDirectory, self).__init__(dom)
        self.add_arg_def([
            Argument('src', Types.String, required=True),
            Argument('dest', Types.String, required=True),
            Argument('timeout', Types.Number),
            Argument('maxTime', Types.Number),
        ])

    def getObject(self):
        return steps.CopyDirectory(**self._args)

    def getSource(self):
        return "steps.CopyDirectory(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(CopyDirectory, self).get_defaults(defaults)
        default_values.update(
            defaults.get("CopyDirectory", {})
        )
        return default_values


class FileUpload(interfaces.Step):
    def __init__(self, dom):
        super(FileUpload, self).__init__(dom)
        self.add_arg_def([
            Argument('workersrc', Types.String, required=True),
            Argument('masterdest', Types.String, required=True),
            Argument('url', Types.String),
            Argument('urlText', Types.String),
        ])

    def getObject(self):
        return steps.FileUpload(**self._args)

    def getSource(self):
        return "steps.FileUpload(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(FileUpload, self).get_defaults(defaults)
        default_values.update(
            defaults.get("FileUpload", {})
        )
        return default_values


class RemoveDirectory(interfaces.Step):
    def __init__(self, dom):
        super(RemoveDirectory, self).__init__(dom)
        self.add_arg_def([
            Argument('dir', Types.String, required=True)
        ])

    def getObject(self):
        return steps.RemoveDirectory(**self._args)

    def getSource(self):
        return "steps.RemoveDirectory(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(RemoveDirectory, self).get_defaults(defaults)
        default_values.update(
            defaults.get("RemoveDirectory", {}))
        return default_values


class MakeDirectory(interfaces.Step):
    def __init__(self, dom):
        super(MakeDirectory, self).__init__(dom)
        self.add_arg_def([
            Argument('dir', Types.String, required=True)
        ])

    def getObject(self):
        return steps.MakeDirectory(**self._args)

    def getSource(self):
        return "steps.MakeDirectory(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(MakeDirectory, self).get_defaults(defaults)
        default_values.update(
            defaults.get("MakeDirectory", {}))
        return default_values


api.DefineBuildStep(FileExists)
api.DefineBuildStep(CopyDirectory)
api.DefineBuildStep(FileUpload)
api.DefineBuildStep(RemoveDirectory)
api.DefineBuildStep(MakeDirectory)

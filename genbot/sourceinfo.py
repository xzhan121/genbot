from __future__ import absolute_import, division, print_function

import buildbot.util as util
import json


class SourceInfo(object):
    def __init__(self, name, repo, branch, yaml_file):
        tmp = util.giturlparse(repo)

        self.name = name
        self.repo = repo
        self.branch = branch
        self.file = yaml_file

        self.owner = tmp.owner
        self.reponame = tmp.repo

    @property
    def id(self):
        return "{name}:{owner}:{reponame}:{branch}:{file}".format(
            name=self.name,
            owner=self.owner,
            reponame=self.reponame,
            branch=self.branch,
            file=self.file
        )

    def asJson(self):
        return json.dumps(
            dict(
                name=self.name,
                repo=self.repo,
                branch=self.branch,
                yaml_file=self.file
            )
        )

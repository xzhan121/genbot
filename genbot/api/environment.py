from __future__ import absolute_import, division, print_function
import genbot.glb as glb
import genbot.interfaces as interfaces


def DefineEnvironment(cls, name=None):
    '''
    Add Step object to known steps we can use.
    '''

    if name is None:
        name = cls.__name__

    if not issubclass(cls, interfaces.Environment):
        raise RuntimeError("Object must be a subclass of Environment")

    glb.known_environments[name] = cls


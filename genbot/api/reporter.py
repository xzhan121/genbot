from __future__ import absolute_import, division, print_function
import genbot.glb as glb
import genbot.interfaces as interfaces

def DefineReporter(cls, name=None):
    '''
    Add Step object to known steps we can use.
    '''

    if name is None:
        name = cls.__name__

    if not issubclass(cls, interfaces.Reporter):
        raise RuntimeError("Object must be a subclass of Reporter")

    glb.known_reporters[name] = cls
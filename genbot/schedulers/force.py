
from __future__ import absolute_import, division, print_function

import genbot.interfaces as interfaces
import genbot.api as api
import genbot.common.is_a as is_a
import genbot.common.string_utils as string_utils

from genbot.argument import Types, Argument


def paramsAsSource(parameters, indent=0):
    ret = ''

    for param in parameters:
        # print(param)
        for paramType, attrs in param.items():
            if paramType == 'String':
                ret += StringParameter(attrs).asSource(indent)
            elif paramType == 'Fixed':
                ret += FixedParameter(attrs).asSource(indent)
            elif paramType == 'Nested':
                ret += NestedParameter(attrs).asSource(indent)
            else:
                pass

    return ret


def argsAsSource(args, indent):
    ret = ''

    for k, v in args.items():
        ret += spaces(indent)

        if is_a.String(v):
            ret += "{k}='{v}',\n".format(k=k, v=v)
        else:
            ret += "{k}={v},\n".format(k=k, v=v)

    return ret

def spaces(indent):
    return '  ' * indent


class ForceScheduler(interfaces.Schedule):

    def __init__(self, dom):
        super(ForceScheduler, self).__init__(dom)
        self.add_arg_def([
            Argument('name', [Types.String], required=True),
            Argument('buttonName', [Types.String]),
            Argument('builder', [Types.String], required=True), # at the moment only support using 1 builder type
            Argument('pipeline', [Types.String], required=True),
            Argument('label', [Types.String]),
            Argument('properties', [Types.Array]),  # TODO required or no?
            Argument('codebases', [Types.StringArray]),  # TODO required or no?
            Argument('reason', [Types.Any])
        ])

        self._name = None
        self._builders = None
        self._pipeline = None
        self._label = None
        self._properties = None
        self._codebases = None
        self.__indent = 1  # we start with 1 level of indentation

    def parse(self, obj, id=None):
        print("HELLO")
        print(self.buildinfo.repo)
        print(self.buildinfo.branch)
        for key, value in obj.items():
            print(key, value)
            tmp = self.verify_args(key, value)

            if tmp:
                if tmp.ArgName == "name":
                    self._args[tmp.ArgName] = value
                elif tmp.ArgName == "label":
                    self._args[tmp.ArgName] = value
                elif tmp.ArgName == "buttonName":
                    self._args[tmp.ArgName] = value
                elif tmp.ArgName == "builder":
                    self._builders = value
                elif tmp.ArgName == "pipeline":
                    self._pipeline = value
                elif tmp.ArgName == "properties":
                    self._properties = value
                elif tmp.ArgName == "codebases":
                    self._args[tmp.ArgName] = value

    def getSource(self):
        ret = ""

        ret += "schedulers.ForceScheduler(\n"
        ret += argsAsSource(self._args, self.__indent + 1) + '\n'
        ret += spaces(self.__indent + 1) + 'builderNames=["{0}"],\n'.format(self._builders)

        ret += spaces(self.__indent + 1) + 'properties=[\n'
        ret += paramsAsSource(self._properties, indent=self.__indent + 2)
        # ret += spaces(3) + 'util.FixedParameter(name="buildinfo", default='
        ret += spaces(self.__indent + 1) + '],\n'

        # ret += spaces(2) + 'codebases=[\n'

        ret += spaces(self.__indent) + "),\n"
        return ret


class ParameterBase(object):

    def __init__(self, yaml):
        self._yaml_data = yaml

    def asSource(self, indent):
        ret = spaces(indent)
        ret += self._className + '(\n'
        ret += argsAsSource(self._yaml_data, indent + 1) + '\n'
        ret += spaces(indent) + '),\n'

        return ret


class NestedParameter(ParameterBase):

    def __init__(self, yaml):
        super(NestedParameter, self).__init__(yaml)

    def argsAsSource(self, indent):
        ret = ''

        for k, v in self._yaml_data.items():
            if k == 'fields':
                continue

            ret += spaces(indent)
            if is_a.String(v):
                ret += "{k}='{v}',\n".format(k=k, v=v)
            else:
                ret += "{k}={v},\n".format(k=k, v=v)

        return ret

    def asSource(self, indent):
        ret = spaces(indent)
        ret += 'util.NestedParameter(\n'
        ret += self.argsAsSource(indent + 1) + '\n'
        ret += spaces(indent + 1) + 'fields=[\n'
        ret += paramsAsSource(self._yaml_data['fields'], indent + 2) + '\n'
        ret += spaces(indent + 1) + '],\n'
        ret += spaces(indent) + '),\n'

        return ret


class StringParameter(ParameterBase):
    # TODO: not complete class implementation

    def __init__(self, yaml):
        super(StringParameter, self).__init__(yaml)
        self._className = 'util.StringParameter'


class FixedParameter(ParameterBase):

    def __init__(self, yaml):
        super(FixedParameter, self).__init__(yaml)
        self._className = 'util.FixedParameter'


api.DefineScheduler(ForceScheduler)
